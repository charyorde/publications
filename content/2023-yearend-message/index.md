---
title: "2023 Year End Message to our Clients and Partners"
date: 2023-12-24T11:40:14+02:00
publishdate: 2023-12-24T11:40:14+02:00
image: "/images/blog/4.jpg"
tags: ["performance report", "annual letter", "blog"]
comments: false
---
Thank you to our esteemed Clients and Partners for the patience and trust in us
to deliver quality services and help them achieve their goals.

Our business grew by 407% across home subscription, brokerage & marketable
securities, private credit, and managed infrastructure.

##### Private Credit

Early in the year we understood how difficult the year was going to be due to high interest rates. Our [private credit business][private-credit] grew by 60% covering healthcare, payments and cloud computing. We grew our payments and credit business by lending to small businesses and covering the FX needs through asset and specialized financing at favourable and competitive interest rates.

[private-credit]: https://publications.huntrecht.com/private-credit/

##### Brokerage & Marketable Securities

Marketable securities grew by 95% due to our exposure to [Payments business][payments-business]. We added FX brokerage to serve our B2B cross-border payments customers. Our
strategic approach which involves premium membership and customer experience helps us to serve and meet client needs.

[payments-business]: https://publications.huntrecht.com/payment-aggregation/

![Marketable Securities](images/dangote-cement-valuation.png "Marketable Securities")

We chased a few whales which we had to refund client funds due to misalignment of interests and client lack of patience. We typically place money back
guarantees in our business dealings so that our managers can think and act well while executing their responsibilities. We believe this is the only way to
deliver quality projects to our clients.

##### Home Subscription

Our home subscription business grew by 45%. We added 50 new units to our inventory from our partner real estate developers. We coverted new banking,
money lending and insurance partners as added services to our members and homeowners.

![Home Subscription](images/homebuilders-comps.png "Home Subscription")

##### Managed Infrastructure

In the new year 2024, we will be investing more into our managed infrastructure business covering AI & Cloud computing. Through our line of credit provided by
our banking partners, we will be supporting businesses with high speed technology operations across travels, hospitality and financial services to
bring increased efficiency to serve critical business operations.

We continue to look out for great businesses and management teams through our partner and affiliate network.

For those interested, we're actively selling preferred stock in our brokerage and trade financing business. For interested clients, please [get a copy][subscription] or email us at ir@huntrecht.com

[subscription]: https://statics.huntrecht.com/docs/Master-Subscription-Agreement.pdf
