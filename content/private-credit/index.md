---
title: "Private Credit Solutions"
date: 2023-03-17T11:40:14+02:00
publishdate: 2023-03-17T11:40:14+02:00
image: "/images/blog/4.jpg"
tags: ["investment management", "business", "blog"]
comments: false
---
Private credit investments surge by 89% in 2022 to reach $1.2T asset under
management. When the banks shut you out, like SVB and Signature Banks failure, entrepreneurs should look into private
credit offerings. The major thing you don’t want to happen as an entrepreneur is
a total shutdown of your business. Now, I’m talking about real entrepreneurs who
go about their business with real passion and desire to effect change in the
society and affect lives positively.

Private credit generally helps during inflationary periods as founders and
entrepreneurs have various refinancing options and downside risk protection
tools to leverage upon such as floating-rate instruments. It is also during these challenging periods that
enterpreneurs create products that solve customer problems, hence, are willing
to pay for.

![Private Credit dry powder](images/Private-credit-dry-powder.png "Private Credit dry powder")

##### The Offering
Some of our existing customers call it impact investing, long term overdraft,
some call it a revolver or venture debt. To us, it’s simply money available to
you everytime you need it over the long term - whether it’s for CAPEX or OPEX.
As long as we understand your business model and strategy, funds are released
over a long term in various tranches, delivering positive cashflow and operating
income.

When equity doesn't come cheap any longer, use debt to win your opportunities,
engineer your products, price it smartly, sell aggressively and grow your assets
to augment these liabilities thereby boosting your working capital. 

> The best results occur at companies that require minimal assets to conduct high-margin businesses—and offer goods or services that will expand their sales volume with only minor needs for additional capital. — Warren Buffett

> Survival is not running out of money until you get product into the market -Steve Jobs 

You can call this a private credit solution for businesses that need $250K -
$10M to cover for research & development, engineering, healthcare, trade financing, strategic acquisition
financing, mortgage and construction financing needs.

Imagine your business is generating $300K annually in revenues and you’re
offered a $500K debt at $2,500 monthly interest payment over a 5 - 10 year
period with a 6 - 12 months interest holiday (zero interest payments). So every
year, you get 25% of $500K cheque to run your business, try new things and grow
your revenues and customer base.

##### Enterpreneurs need to consider Private Credit
- When the business is growing but not fast enough to get new equity investors
    interested.
- When unit economics actually work but there is a valuation gap or management
    does not want to be diluted further
- When the business is close to profitability and equity is too expensive or
    will take too long to raise.

##### What does it cost me?
All you have to do is pay upfront a one-time 3.81% fee deducted from your annual
principal repayment.

Another thing to take note of is that we usually follow-on with a substantial
equity investment once element of certainty in the business has been
established.

##### And what's the benefits for me as an entrepreneur...
- Join a highly liquid and experienced business network
- Get support to do what you love without interference from the owners
- Unsecured or non-collaterized financing for your business
- Longer-term nature of private funds commitments
- Reach over 800K trading partners

From the desk of our investment managers, we are here to help. Reach out to us
at finservices@huntrecht.com
