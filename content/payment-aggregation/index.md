---
title: "Delivering Customer Experience through Payment Aggregation"
date: 2023-11-27T11:40:14+02:00
publishdate: 2023-11-27T11:40:14+02:00
image: "/images/blog/4.jpg"
tags: ["payment", "customer experience", "blog"]
comments: false
---
Processing over $450M transaction value on HCL Commerce and IBM OMS digital
commerce platforms globally including South Africa, UAE, UK, Canada, Singapore and Thailand
delivering digital trade, e-commerce and home subscription.

![Adyen IBM OMS Integration](images/adyen-ibm-oms.png "Adyen IBM OMS Integration")

Materials sourced from renewable sources such as mass timber, contribute to carbon emission reduction, making them suitable for various green building applications. While low-carbon construction materials currently have a moderate-to-high impact on end-user industries, their adoption is expected to increase in the future, especially in residential, commercial, industrial and other building and construction applications.

We keep seeing symmetrical movement between building materials and housing market prices across US and UK markets.

![Building materials vs homebuilder index](images/bm-homebuilder-index.png "Building materials vs homebuilder index")

Through our Private Credit Funds that has delivered 60% till date,
across homes, hospitality and supply chain. For us, this is a CX
Fund as we're committed to delivering world class customer experience to our
customers.

![Resilient Payment](images/resilient-payment.jpg "Resilient Payment")

Our payment agggregation offering is a CX offering with more than 10 payment
processors integrated through a RL-based selection process determined by
selected payment features from each provider.

We enabled pre-authorization in partnership with selected banks for concierge
experiences in hospitality, restaurants, malls and entertainment centers.

[Onboard today][order-today]

[order-today]: https://statics.huntrecht.com/docs/Form_Trade_Mandate_Onboarding_ZAR.pdf

From the desk of our investment managers, we are here to help. Reach out to us
at finservices@huntrecht.com
