---
title: "Global Technology Financing"
date: 2024-12-29T11:40:14+02:00
publishdate: 2024-12-29T11:40:14+02:00
image: "/images/blog/4.jpg"
tags: ["performance report", "annual letter", "blog"]
comments: false
---
2024 is a mixed year for us. We firm up our strategies, onboarded new partners including liquidity partners, data center suppliers and most importantly oil & gas.

##### Marketing and Distribution
We signed a $20M distribution contract for CPG across oil & gas products and F&B. We’re building a digital twin to improve distribution efficiency, gain access to hidden insights through data intelligence and help our customers make informed decisions. We expect this to grow our revenues by 50% annually over 4 year period.

We will be consolidating our efforts in 2025 to increase our warehouse capacity for an improved supply chain.

##### Oilfield Services AI Business
With our partnership with [Controlled Investments][controlledcap], we invested in oil & gas assets.

Our investments in oil & gas to [digitize oil field services][oilfieldservicesai] by crunching data to discover new oil and gas reservoirs as well as to perform calculations to advance clean energy. We can’t achieve this without investments in supercomputers with GPU clusters. With [acquisition of oil tools and well testing][fwt-press] businesses added to our portfolio, we’re able to provide a full lifecycle for Digital Twin of oil & gas wells.

[controlledcap]: https://www.controlledcap.com
[oilfieldservicesai]: https://www.oilfieldservices.ai/
[fwt-press]: https://www.stocktitan.net/news/AZRH/freedom-well-testing-joins-azure-holding-group-azrh-in-strategic-w9e9q6zjo3wg.html

![Oil & Gas Acquisitions](images/oilandgasacquisitions.png "Oil & Gas Acquisitions")

##### FX and Trade Settlement
Our international trade business is heavily exposed to currency fluctuations, high interest rates and risky local currency exposure. We try to buy assets such as stablecoin when the markets is in our favour so that we can profit from the market risks we take.

![Stablecoin-backed Guarantee](images/crypto-backed-guarantees.jpg "Stablecoin-backed Guarantee")

Distributing products across emerging markets? Partner with us. Zero upfront fees, over 3 million trading partners, multicurrency settlement and liquidity providers.

Order your next oil & gas products, heavy equipment, servers and GPUs financed through Global Financing

